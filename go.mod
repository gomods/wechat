module gitee.com/gomods/wechat

go 1.15

require (
	gitee.com/gomods/logger v0.0.2
	gitee.com/gomods/redisdao v0.0.2
	github.com/mitchellh/mapstructure v1.4.1
)
