package request

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"

	"gitee.com/gomods/logger"
)

type Request struct {
	http        *http.Client
	contentType ContentType
}

func NewRequest(http *http.Client, ctp ContentType) *Request {
	return &Request{
		http:        http,
		contentType: ctp,
	}
}

func (cli *Request) Get(ctx context.Context, url string, response interface{}) error {
	tag := "wechat.request.get"

	logger.Ix(ctx, tag, "http get request record", "url", url)

	resp, err := cli.http.Get(url)
	if err != nil {
		logger.Ex(ctx, tag, "http get request error", "url", url, "error", err)
		return err
	}
	defer resp.Body.Close()

	switch cli.contentType {
	case ContentTypeXML:
		return xml.NewDecoder(resp.Body).Decode(response)
	case ContentTypeJSON:
		return json.NewDecoder(resp.Body).Decode(response)
	default:
		return errors.New("invalid content type")
	}
}

func (cli *Request) GetWithBody(ctx context.Context, url string) (*http.Response, error) {
	tag := "wechat.request.getWithBody"

	logger.Ix(ctx, tag, "http get request record", "url", url)
	rsp, err := cli.http.Get(url)
	if err != nil {
		logger.Ex(ctx, tag, "http get request with body error", "url", url, "error", err)
		return nil, err
	}

	return rsp, nil
}

func (cli *Request) Post(ctx context.Context, url string, params interface{}, response interface{}) error {
	tag := "wechat.request.post"

	logger.Ix(ctx, tag, "http post request record", "url", url, "params", fmt.Sprintf("%+v", params))

	resp, err := cli.PostWithBody(ctx, url, params)
	if err != nil {
		logger.Ex(ctx, tag, "http post request with body error", "url", url, "params", fmt.Sprintf("%+v", params), "error", err)
		return err
	}
	defer resp.Body.Close()

	switch cli.contentType {
	case ContentTypeXML:
		return xml.NewDecoder(resp.Body).Decode(response)
	case ContentTypeJSON:
		return json.NewDecoder(resp.Body).Decode(response)
	default:
		return errors.New("invalid content type")
	}
}

func (cli *Request) PostWithBody(ctx context.Context, url string, params interface{}) (*http.Response, error) {
	tag := "wechat.request.postWithBody"

	logger.Ix(ctx, tag, "http post request record", "url", url, "params", fmt.Sprintf("%+v", params))
	buf := new(bytes.Buffer)
	if params != nil {
		switch cli.contentType {
		case ContentTypeXML:
			err := xml.NewEncoder(buf).Encode(params)
			if err != nil {
				return nil, err
			}
		case ContentTypeJSON:
			enc := json.NewEncoder(buf)
			enc.SetEscapeHTML(false)
			err := enc.Encode(params)
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid content type")
		}
	}

	rsp, err := cli.http.Post(url, cli.contentType.String(), buf)
	if err != nil {
		logger.Ex(ctx, tag, "http post request with body error", "url", url, "params", fmt.Sprintf("%+v", params), "error", err)
		return nil, err
	}

	return rsp, nil
}

func (cli *Request) FormPostWithFile(ctx context.Context, url, field, filename string, response interface{}) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	return cli.FormPost(ctx, url, field, filename, file, response)
}

func (cli *Request) FormPost(ctx context.Context, url, field, filename string, reader io.Reader, response interface{}) error {
	tag := "wechat.request.formPost"

	logger.Ix(ctx, tag, "http form post request record", "url", url)

	// Prepare a form that you will submit to that URL.
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	fw, err := w.CreateFormFile(field, filename)
	if err != nil {
		return err
	}

	if _, err = io.Copy(fw, reader); err != nil {
		return err
	}

	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()

	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", url, buf)
	if err != nil {
		return err
	}
	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", w.FormDataContentType())

	resp, err := cli.http.Do(req)
	if err != nil {
		logger.Ex(ctx, tag, "http form post request error", "url", url, "error", err)
		return err
	}
	defer resp.Body.Close()

	switch cli.contentType {
	case ContentTypeXML:
		return xml.NewDecoder(resp.Body).Decode(response)
	case ContentTypeJSON:
		return json.NewDecoder(resp.Body).Decode(response)
	default:
		return errors.New("invalid content type")
	}
}
