package search

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiImageSearch = "/wxa/imagesearch"

type CreateActivityIDResponse struct {
	request.CommonError
	// 生成的小程序 URL Link
	URLLink string `json:"url_link"`
}

// 本接口提供基于小程序的站内搜商品图片搜索能力
func (cli *Search) ImageSearch(ctx context.Context, filename string) (*CreateActivityIDResponse, error) {

	url, err := cli.conbineURI(ctx, apiImageSearch, nil, true)
	if err != nil {
		return nil, err
	}

	rsp := new(CreateActivityIDResponse)
	if err := cli.request.FormPostWithFile(ctx, url, "img", filename, rsp); err != nil {
		return nil, err
	}

	return rsp, nil
}
