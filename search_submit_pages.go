package wechat

import (
	"context"
	"gitee.com/gomods/wechat/request"
)

const (
	apiSearchSubmitPages = "/wxa/search/wxaapi_submitpages"
)

// SearchSubmitPagesRequest 小程序页面收录请求
type SearchSubmitPagesRequest struct {
	Pages []SearchSubmitPage `json:"pages"`
}

// SearchSubmitPage 请求收录的页面
type SearchSubmitPage struct {
	Path  string `json:"path"`
	Query string `json:"query"`
}

// Send 提交收录请求
func (cli *Client) SendSearchSubmitPages(ctx context.Context, smp *SearchSubmitPagesRequest) (*request.CommonError, error) {
	api := baseURL + apiSearchSubmitPages
	token, err := cli.AccessToken(ctx)
	if err != nil {
		return nil, err
	}

	return cli.sendSearchSubmitPages(ctx, api, token, smp)
}

func (cli *Client) sendSearchSubmitPages(ctx context.Context, api, token string, smp *SearchSubmitPagesRequest) (*request.CommonError, error) {
	api, err := tokenAPI(api, token)
	if err != nil {
		return nil, err
	}

	res := new(request.CommonError)
	if err := cli.request.Post(ctx, api, smp, res); err != nil {
		return nil, err
	}

	return res, nil
}
