package wechat

import (
	"context"
	"errors"
	"net/http"
	"time"

	"gitee.com/gomods/wechat/auth"
	"gitee.com/gomods/wechat/cache"
	"gitee.com/gomods/wechat/livebroadcast"
	"gitee.com/gomods/wechat/ocr"
	"gitee.com/gomods/wechat/operation"
	"gitee.com/gomods/wechat/phonenumber"
	"gitee.com/gomods/wechat/request"
	"gitee.com/gomods/wechat/search"
	"gitee.com/gomods/wechat/security"
	"gitee.com/gomods/wechat/server"
	"gitee.com/gomods/wechat/subscribemessage"
	"gitee.com/gomods/wechat/updatablemessage"
	"gitee.com/gomods/wechat/wxacode"

	"github.com/mitchellh/mapstructure"
)

const (
	// baseURL 微信请求基础URL
	baseURL = "https://api.weixin.qq.com"
)

type Client struct {
	// HTTP请求客户端
	request *request.Request
	// 数据缓存器
	cache cache.Cache
	// 小程序后台配置: 小程序ID
	appid string
	// 小程序后台配置: 小程序密钥
	secret string
	// 用户自定义获取access_token的方法
	accessTokenGetter AccessTokenGetter
}

// 用户自定义获取access_token的方法
type AccessTokenGetter func() (token string, expireIn uint)

// 初始化客户端并用自定义配置替换默认配置
func NewClient(appid, secret string, opts ...func(*Client)) *Client {
	cli := &Client{
		appid:  appid,
		secret: secret,
	}

	// 执行额外的配置函数
	for _, fn := range opts {
		fn(cli)
	}

	if cli.cache == nil {
		cli.cache = cache.NewRedisCache()
	}

	if cli.request == nil {
		cli.request = request.NewRequest(http.DefaultClient, request.ContentTypeJSON)
	}

	return cli
}

// 自定义 HTTP Client
func WithHttpClient(hc *http.Client) func(*Client) {
	return func(cli *Client) {
		cli.request = request.NewRequest(hc, request.ContentTypeJSON)
	}
}

// 自定义缓存
func WithCache(cc cache.Cache) func(*Client) {
	return func(cli *Client) {
		cli.cache = cc
	}
}

// 自定义获取access_token的方法
func WithAccessTokenSetter(getter AccessTokenGetter) func(*Client) {
	return func(cli *Client) {
		cli.accessTokenGetter = getter
	}
}

// POST 参数
type requestParams map[string]interface{}

// URL 参数
type requestQueries map[string]interface{}

// tokenAPI 获取带 token 的 API 地址
func tokenAPI(api, token string) (string, error) {
	queries := requestQueries{
		"access_token": token,
	}

	return request.EncodeURL(api, queries)
}

// convert bool to int
func bool2int(ok bool) uint8 {

	if ok {
		return 1
	}

	return 0
}

// 获取小程序全局唯一后台接口调用凭据（access_token）。
// 调调用绝大多数后台接口时都需使用 access_token，开发者需要进行妥善保存，注意缓存。
func (cli *Client) AccessToken(ctx context.Context) (string, error) {
	key := cli.tokenCacheKey()
	data, ok := cli.cache.Get(ctx, key)
	if ok {
		return data.(string), nil
	}

	if cli.accessTokenGetter != nil {
		token, expireIn := cli.accessTokenGetter()
		cli.cache.Set(ctx, key, token, time.Duration(expireIn)*time.Second)
		return token, nil
	}

	if cli.cache.Lock(ctx, cli.tokenLockKey()) {
		req := auth.GetAccessTokenRequest{
			Appid:     cli.appid,
			Secret:    cli.secret,
			GrantType: "client_credential",
		}
		rsp, err := cli.NewAuth().GetAccessToken(ctx, &req)
		if err != nil {
			return "", err
		}

		if err := rsp.GetResponseError(); err != nil {
			return "", err
		}
		cli.cache.Set(ctx, key, rsp.AccessToken, time.Duration(rsp.ExpiresIn-1200)*time.Second)
		cli.cache.UnLock(ctx, cli.tokenLockKey())
		return rsp.AccessToken, nil
	} else {
		// Token过期，且未拿到获取锁的情况下，等待1秒后，再次从缓存中拿取
		time.Sleep(time.Duration(1) * time.Second)

		data, ok := cli.cache.Get(ctx, key)
		if ok {
			return data.(string), nil
		}

		return "", errors.New("token is nil")
	}
}

// 拼凑完整的 URI
func (cli *Client) conbineURI(ctx context.Context, url string, req interface{}, withToken bool) (string, error) {

	output := make(map[string]interface{})

	config := &mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   &output,
		TagName:  "query",
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return "", err
	}

	err = decoder.Decode(req)
	if err != nil {
		return "", err
	}

	if withToken {
		token, err := cli.AccessToken(ctx)
		if err != nil {
			return "", err
		}

		output["access_token"] = token
	}

	return request.EncodeURL(baseURL+url, output)
}

// 用户信息
func (cli *Client) NewAuth() *auth.Auth {
	return auth.NewAuth(cli.request, cli.conbineURI)
}

// 微信通知监听服务
func (cli *Client) NewServer(token, aesKey, mchID, apiKey string, validate bool, handler func(map[string]interface{}) map[string]interface{}) (*server.Server, error) {
	return server.NewServer(cli.appid, token, aesKey, mchID, apiKey, validate, handler)
}

// 订阅消息
func (cli *Client) NewSubscribeMessage() *subscribemessage.SubscribeMessage {
	return subscribemessage.NewSubscribeMessage(cli.request, cli.conbineURI)
}

// 运维中心
func (cli *Client) NewOperation() *operation.Operation {
	return operation.NewOperation(cli.request, cli.conbineURI)
}

// 小程序码
func (cli *Client) NewWXACode() *wxacode.WXACode {
	return wxacode.NewWXACode(cli.request, cli.conbineURI)
}

// OCR
func (cli *Client) NewOCR() *ocr.OCR {
	return ocr.NewOCR(cli.request, cli.conbineURI)
}

// 动态消息
func (cli *Client) NewUpdatableMessage() *updatablemessage.UpdatableMessage {
	return updatablemessage.NewUpdatableMessage(cli.request, cli.conbineURI)
}

// 小程序搜索
func (cli *Client) NewSearch() *search.Search {
	return search.NewSearch(cli.request, cli.conbineURI)
}

// 直播
func (cli *Client) NewLiveBroadcast() *livebroadcast.LiveBroadcast {
	return livebroadcast.NewLiveBroadcast(cli.request, cli.conbineURI)
}

// 内容安全
func (cli *Client) NewSecurity() *security.Security {
	return security.NewSecurity(cli.request, cli.conbineURI)
}

// 手机号
func (cli *Client) NewPhonenumber() *phonenumber.Phonenumber {
	return phonenumber.NewPhonenumber(cli.request, cli.conbineURI)
}
