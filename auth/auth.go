package auth

import (
	"context"
	"gitee.com/gomods/wechat/request"
)

// 用户信息
type Auth struct {
	request *request.Request
	// 组成完整的 URL 地址
	// 默认包含 AccessToken
	conbineURI func(ctx context.Context, url string, req interface{}, withToken bool) (string, error)
}

func NewAuth(request *request.Request, conbineURI func(ctx context.Context, url string, req interface{}, withToken bool) (string, error)) *Auth {
	sm := Auth{
		request:    request,
		conbineURI: conbineURI,
	}

	return &sm
}
