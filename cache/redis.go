package cache

import (
	"context"
	"time"

	"gitee.com/gomods/logger"
	"gitee.com/gomods/redisdao"
)

type redisCache struct {
}

func NewRedisCache() Cache {
	rds := redisCache{}

	return &rds
}

func (c *redisCache) Get(ctx context.Context, key string) (interface{}, bool) {
	client := redisdao.NewSimpleRedis("redis")

	item, err := client.Get(ctx, key).Result()
	if err != nil {
		logger.Ex(context.Background(), "wechat.redisCache.get", "get access token error", "key", key, "error", err)
		return nil, false
	}

	return item, true
}

func (c *redisCache) Set(ctx context.Context, key string, val interface{}, timeout time.Duration) {
	client := redisdao.NewSimpleRedis("redis")

	_, err := client.Set(ctx, key, val, timeout).Result()
	if err != nil {
		logger.Ex(context.Background(), "wechat.redisCache.set", "set access token error", "key", key, "error", err, "access_token", val)
	}
}

func (c *redisCache) Delete(ctx context.Context, key string) error {
	client := redisdao.NewSimpleRedis("redis")

	_, err := client.Del(ctx, key).Result()
	if err != nil {
		logger.Ex(ctx, "wechat.redisCache.del", "del access token error", "key", key, "error", err)
		return err
	}

	return nil
}

func (c *redisCache) Lock(ctx context.Context, key string) bool {
	client := redisdao.NewSimpleRedis("redis")
	ret, err := client.SetNX(context.Background(), key, "1", time.Duration(3)*time.Second).Result()
	if err != nil {
		logger.Ex(ctx, "wechat.redisCache.lock", "lock access token error", "key", key, "error", err)
		return false
	}

	return ret
}

func (c *redisCache) UnLock(ctx context.Context, key string) error {
	client := redisdao.NewSimpleRedis("redis")

	_, err := client.Del(context.Background(), key).Result()
	if err != nil {
		logger.Ex(ctx, "wechat.redisCache.unlock", "unlock access token error", "key", key, "error", err)
		return err
	}

	return nil
}
