package cache

import (
	"context"
	"time"
)

type Cache interface {
	// 存储数据
	Set(ctx context.Context, key string, val interface{}, timeout time.Duration)
	// 获取数据
	// 返回数据和数据是否存在
	Get(ctx context.Context, key string) (interface{}, bool)
	// 锁
	Lock(ctx context.Context, Key string) bool
	// 释放锁
	UnLock(ctx context.Context, Key string) error
}
