package cache

import (
	"context"
	"sync"
	"time"
)

type memory struct {
	store *sync.Map
}

type memoryItem struct {
	val   interface{}
	timer *time.Timer
}

func NewMemoryCache() Cache {
	mem := memory{
		store: new(sync.Map),
	}

	return &mem
}

func (c *memory) Get(ctx context.Context, key string) (interface{}, bool) {
	item, ok := c.store.Load(key)
	if !ok {
		return nil, false
	}

	return item.(memoryItem).val, true
}

func (c *memory) Set(ctx context.Context, key string, val interface{}, timeout time.Duration) {
	timer := time.AfterFunc(timeout, func() {
		_ = c.Delete(ctx, key)

	})

	item := memoryItem{val, timer}

	c.store.Store(key, item)
}

func (c *memory) Delete(ctx context.Context, key string) error {
	c.store.Delete(key)
	return nil
}

func (c *memory) Lock(ctx context.Context, key string) bool {
	return true
}

func (c *memory) UnLock(ctx context.Context, key string) error {
	return nil
}
