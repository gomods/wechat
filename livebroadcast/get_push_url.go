package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiGetPushUrl = "/wxaapi/broadcast/room/getpushurl"

type GetPushUrlRequest struct {
	// 必填	直播间id
	RoomId int64 `query:"roomId"`
}

type GetPushUrlResponse struct {
	request.CommonError
	// 直播间推流地址
	PushAddr string `json:"pushAddr"`
}

// 获取直播间推流地址
func (cli *LiveBroadcast) GetPushUrl(ctx context.Context, req *GetPushUrlRequest) (*GetPushUrlResponse, error) {

	api, err := cli.conbineURI(ctx, apiGetPushUrl, req, true)
	if err != nil {
		return nil, err
	}

	res := new(GetPushUrlResponse)
	err = cli.request.Get(ctx, api, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
