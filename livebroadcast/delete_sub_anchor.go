package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiDeleteSubAnchor = "/wxaapi/broadcast/room/deletesubanchor"

type DeleteSubAnchorRequest struct {
	// 必填	房间ID
	RoomId int64 `json:"roomId"`
}

// 删除主播副号
func (cli *LiveBroadcast) DeleteSubAnchor(ctx context.Context, req *DeleteSubAnchorRequest) (*request.CommonError, error) {

	api, err := cli.conbineURI(ctx, apiDeleteSubAnchor, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(request.CommonError)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
