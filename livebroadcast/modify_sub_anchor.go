package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiModifySubAnchor = "/wxaapi/broadcast/room/modifysubanchor"

type ModifySubAnchorRequest struct {
	// 必填 房间ID
	RoomId int64 `json:"roomId"`
	// 必填	用户微信号
	Username string `json:"username"`
}

type ModifySubAnchorResponse struct {
	request.CommonError
}

// 修改主播副号
func (cli *LiveBroadcast) ModifySubAnchor(ctx context.Context, req *ModifySubAnchorRequest) (*ModifySubAnchorResponse, error) {

	api, err := cli.conbineURI(ctx, apiModifySubAnchor, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(ModifySubAnchorResponse)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
