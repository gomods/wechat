package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiUpdateComment = "/wxaapi/broadcast/room/updatecomment"

type UpdateCommentRequest struct {
	// 必填 房间ID
	RoomId int64 `json:"roomId"`
	// 必填	1-禁言，0-取消禁言
	BanComment uint8 `json:"banComment"`
}

type UpdateCommentResponse struct {
	request.CommonError
}

// 开启/关闭直播间全局禁言
func (cli *LiveBroadcast) UpdateComment(ctx context.Context, req *UpdateCommentRequest) (*UpdateCommentResponse, error) {

	api, err := cli.conbineURI(ctx, apiUpdateComment, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(UpdateCommentResponse)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
