package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiGetSharedCode = "/wxaapi/broadcast/room/GetSharedCode"

type GetSharedCodeRequest struct {
	//	房间ID
	RoomId int64 `query:"roomId"`
	// 自定义参数
	Params int64 `query:"params"`
}

type GetSharedCodeResponse struct {
	request.CommonError
	// 分享二维码地址
	CdnUrl string `json:"cdnUrl"`
	// 分享路径
	PagePath string `json:"pagePath"`
	// 分享海报地址
	PosterUrl string `json:"posterUrl"`
}

// 获取直播间分享二维码
func (cli *LiveBroadcast) GetSharedCode(ctx context.Context, req *GetSharedCodeRequest) (*GetSharedCodeResponse, error) {

	api, err := cli.conbineURI(ctx, apiGetSharedCode, req, true)
	if err != nil {
		return nil, err
	}

	res := new(GetSharedCodeResponse)
	err = cli.request.Get(ctx, api, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
