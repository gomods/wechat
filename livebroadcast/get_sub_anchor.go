package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiGetSubAnchor = "/wxaapi/broadcast/room/GetSubAnchor"

type GetSubAnchorRequest struct {
	//	房间ID
	RoomId int64 `query:"roomId"`
}

type GetSubAnchorResponse struct {
	request.CommonError
	Username string `json:"username"`
}

// 获取主播副号
func (cli *LiveBroadcast) GetSubAnchor(ctx context.Context, req *GetSubAnchorRequest) (*GetSubAnchorResponse, error) {

	api, err := cli.conbineURI(ctx, apiGetSubAnchor, req, true)
	if err != nil {
		return nil, err
	}

	res := new(GetSubAnchorResponse)
	err = cli.request.Get(ctx, api, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
