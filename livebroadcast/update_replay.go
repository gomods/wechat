package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiUpdateReplay = "/wxaapi/broadcast/room/updatereplay"

type UpdateReplayRequest struct {
	// 必填 房间ID
	RoomId int64 `json:"roomId"`
	// 必填	是否关闭回放 【0：开启，1：关闭】
	CloseReplay uint8 `json:"closeReplay"`
}

type UpdateReplayResponse struct {
	request.CommonError
}

// 开开启/关闭回放功能
func (cli *LiveBroadcast) UpdateReplay(ctx context.Context, req *UpdateReplayRequest) (*UpdateReplayResponse, error) {

	api, err := cli.conbineURI(ctx, apiUpdateReplay, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(UpdateReplayResponse)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
