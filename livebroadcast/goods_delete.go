package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiGoodsDelete = "/wxaapi/broadcast/goods/delete"

type GoodsDeleteRequest struct {
	// 商品ID
	GoodsId int64 `json:"goodsId"`
}

type GoodsDeleteResponse struct {
	request.CommonError
}

// 删除商品
func (cli *LiveBroadcast) GoodsDelete(ctx context.Context, req *GoodsDeleteRequest) (*GoodsDeleteResponse, error) {

	api, err := cli.conbineURI(ctx, apiGoodsDelete, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(GoodsDeleteResponse)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
