package livebroadcast

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiModifyAssistant = "/wxaapi/broadcast/room/modifyassistant"

type ModifyAssistantRequest struct {
	// 必填 房间ID
	RoomId int64 `json:"roomId"`
	// 必填	用户微信号
	Username string `json:"username"`
	// 必填	用户微信昵称
	Nickname string `json:"nickname"`
}

type ModifyAssistantResponse struct {
	request.CommonError
}

// 修改管理直播间小助手
func (cli *LiveBroadcast) ModifyAssistant(ctx context.Context, req *ModifyAssistantRequest) (*ModifyAssistantResponse, error) {

	api, err := cli.conbineURI(ctx, apiModifyAssistant, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(ModifyAssistantResponse)
	err = cli.request.Post(ctx, api, req, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
