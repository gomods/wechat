package livebroadcast

import (
	"context"
	"gitee.com/gomods/wechat/request"
)

type LiveBroadcast struct {
	request *request.Request
	// 组成完整的 URL 地址
	// 默认包含 AccessToken
	conbineURI func(ctx context.Context, url string, req interface{}, withToken bool) (string, error)
}

func NewLiveBroadcast(request *request.Request, conbineURI func(ctx context.Context, url string, req interface{}, withToken bool) (string, error)) *LiveBroadcast {
	sm := LiveBroadcast{
		request:    request,
		conbineURI: conbineURI,
	}

	return &sm
}
