package subscribemessage

import (
	"context"

	"gitee.com/gomods/wechat/request"
)

const apiGetCategory = "/wxaapi/newtmpl/getcategory"

type GetCategoryResponse struct {
	request.CommonError
	Data []struct {
		ID   int    `json:"id"`   // 类目id，查询公共库模版时需要
		Name string `json:"name"` // 类目的中文名
	} `json:"data"` // 类目列表
}

// 删除帐号下的某个模板
func (cli *SubscribeMessage) GetCategory(ctx context.Context) (*GetCategoryResponse, error) {
	api, err := cli.conbineURI(ctx, apiGetCategory, nil, true)
	if err != nil {
		return nil, err
	}

	res := new(GetCategoryResponse)
	err = cli.request.Get(ctx, api, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
